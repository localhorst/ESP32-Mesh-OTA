/**
* @file Blinky_LED.h
* @brief Demo application using the mesh network
* @author Hendrik Schutter
* @date 20.01.2021
*
* Additional Infos: If button "BOOT" on ESP32-Module is pressed, all LED2 (blue) in the network will toggle the state.
*/

#ifndef H_BLINKY_LED
#define H_BLINKY_LED

#include <string.h>
#include "esp_wifi.h"
#include "esp_system.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"

#include "Mesh_OTA.h"

#define NEW_VERSION

#define GPIO_BOOT_BTN   0 //GPIO0 (Boot BTN)
#define GPIO_LED_BLUE   2 //GPIO2 (internal blue LED in DevKit V1.0)
#define GPIO_LED_GREEN 13 //GPIO13

#define GPIO_INPUT_PIN_SEL  (1ULL<<GPIO_BOOT_BTN)

struct blinky_packet
{
    enum blinky_packet_type
    {
        LED_OFF,
        LED_ON,
    } type;
    mesh_addr_t meshSenderAddr; //stores addr of sender of this packet
};

typedef struct blinky_packet BLINKY_PACKET_t;

esp_err_t errBlinkyLEDInitialize(void);
void vGPIOInitialize(void);
void rxHandle(const uint8_t* const pu8Data, const uint8_t* const pu8Sender);
void vTaskReadUserInput(void *arg);
void vTaskReceiveData(void *arg);

#endif /* H_BLINKY_LED */


