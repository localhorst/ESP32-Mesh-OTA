#include <stdio.h>
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"

#include "Mesh_OTA.h"
#include "Blinky_LED.h"

static const char *LOG_TAG = "esp_main";

void app_main(void)
{
    esp_err_t err = ESP_OK;

    ESP_LOGI(LOG_TAG, "start mesh network");
    err = errMeshNetworkInitialize();
    ESP_ERROR_CHECK(err);

    ESP_LOGI(LOG_TAG, "start ota");
    err = errMeshOTAInitialize();
    ESP_ERROR_CHECK(err);

    ESP_LOGI(LOG_TAG, "start app");
    err = errBlinkyLEDInitialize();
    ESP_ERROR_CHECK(err);
}


