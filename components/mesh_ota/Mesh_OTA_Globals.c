/**
* @file Mesh_OTA_Globals.c
* @brief global variables unsed in Mesh_OTA
* @author Hendrik Schutter
* @date 21.01.2021
*/

#include "Mesh_OTA_Globals.h"

xQueueHandle queueNodes; //nodes that should be checked for ota update (contains children and parent)
xQueueHandle queueMessageOTA; //mesh ota controll messages like "OTA_Version_Response" "OTA_ACK"

SemaphoreHandle_t bsStartStopServerWorker; //binary semaphore
SemaphoreHandle_t bsOTAProcess; //binary semaphore

//w: errMeshOTAInitialize;
//r: errMeshOTAInitialize; errMeshOtaPartitionAccessHttps; errMeshOtaPartitionAccessMeshReceive;
const esp_partition_t* pOTAPartition; //pointer to ota partition

//w: errMeshOTAInitialize; vMeshOtaTaskOTAWorker; vMeshOtaTaskServerWorker
//r: errMeshOTAInitialize; vMeshOtaTaskOTAWorker; vMeshOtaTaskServerWorker
bool bWantReboot; //flag to signal pending reboot
