/**
* @file HTTPS_Client.h
* @brief Used to download the OTA image from the server
* @author Hendrik Schutter
* @date 20.01.2021
*
* Additional Infos: Connects via HTTPS and HTTPS Basic Auth to the Server.
* Downloads the image in segments
*/

#ifndef H_HTTPS_CLIENT
#define H_HTTPS_CLIENT

#include <string.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_system.h"
#include "esp_netif.h"
#include "mbedtls/platform.h"
#include "mbedtls/net_sockets.h"
#include "mbedtls/esp_debug.h"
#include "mbedtls/ssl.h"
#include "mbedtls/entropy.h"
#include "mbedtls/ctr_drbg.h"
#include "mbedtls/error.h"
#include "mbedtls/certs.h"
#include "esp_crt_bundle.h"

#ifndef CONFIG_OTA_HTTPS_URL
#define CONFIG_OTA_HTTPS_URL "https://exmaple.com/theImage.bin"
#endif

#ifndef CONFIG_OTA_HTTPS_SERVER_PORT
#define CONFIG_OTA_HTTPS_SERVER_PORT "443"
#endif

#ifndef CONFIG_OTA_HTTPS_AUTH
#define CONFIG_OTA_HTTPS_AUTH "base64(user:password)"
#endif

#ifndef CONFIG_OTA_HTTPS_SERVER_COMMON_NAME
#define CONFIG_OTA_HTTPS_SERVER_COMMON_NAME "exmaple.com"
#endif

#define HTTPS_CLIENT_OK 0
#define HTTPS_CLIENT_ERROR -1
#define HTTPS_CLIENT_ERROR_INIT_EMBEDTLS -2
#define HTTPS_CLIENT_ERROR_INIT_CONNECT_TWO_SERVER -3
#define HTTPS_CLIENT_ERROR_INIT_VALIDATE_SERVER -4
#define HTTPS_CLIENT_ERROR_INIT_SEND_REQUEST -5

#define HTTPS_READ_TIMEOUT 1000 //ms

struct HTTPS_Client
{
    mbedtls_entropy_context entropy;
    mbedtls_ctr_drbg_context ctr_drbg;
    mbedtls_ssl_context ssl;
    mbedtls_x509_crt cacert;
    mbedtls_ssl_config conf;
    mbedtls_net_context server_fd;
};

typedef int32_t https_client_ret_t;
typedef struct HTTPS_Client HTTPS_Client_t;

https_client_ret_t errHTTPSClientInitialize(void);
https_client_ret_t errHTTPSClientConnectToServer(void);
https_client_ret_t errHTTPSClientValidateServer(void);
https_client_ret_t errHTTPSClientSendRequest(void);
https_client_ret_t errHTTPSClientRetrieveData(char* const cpu8Data, const uint32_t* const cpcu32DataLenght, uint32_t* pu32BytesRead);
https_client_ret_t errHTTPSClientReset(void);

#endif /* H_HTTPS_CLIENT */




