/**
* @file Mesh_OTA_Globals.h
* @brief global variables unsed in Mesh_OTA
* @author Hendrik Schutter
* @date 21.01.2021
*
*/

#ifndef H_MESH_OTA_GLOBALS
#define H_MESH_OTA_GLOBALS

#include "esp_system.h"
#include "esp_partition.h"

#include "Mesh_Network.h"
#include "HTTPS_Client.h"

extern xQueueHandle queueNodes; //nodes that should be checked for ota update (contains children and parent)
extern xQueueHandle queueMessageOTA; //mesh ota controll messages like "OTA_Version_Response" "OTA_ACK"

extern SemaphoreHandle_t bsStartStopServerWorker; //binary semaphore
extern SemaphoreHandle_t bsOTAProcess; //binary semaphore

extern const esp_partition_t* pOTAPartition; //pointer to ota partition
extern bool bWantReboot; //flag to signal pending reboot

#endif /* H_MESH_OTA_GLOBALS */
