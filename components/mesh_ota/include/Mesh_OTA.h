/**
* @file Mesh_OTA.h
* @brief Start and implement OTA updates via HTTPS from server and other mesh nodes (bidirectional)
* @author Hendrik Schutter
* @date 21.01.2021
*/

#ifndef H_MESH_OTA
#define H_MESH_OTA

#include "esp_system.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_ota_ops.h"
#include "esp_partition.h"

#include "Mesh_Network.h"
#include "HTTPS_Client.h"

#define ERASE_NVS //erase non volatile storage if full
#define QUEUE_NODES_SIZE 20
#define QUEUE_MESSAGE_OTA_SIZE 20
#define SERVER_CHECK_INTERVAL 30 //in seconds
#define OTA_HTTPS_SEGMENT_SIZE 2048U
#define OTA_PROGRESS_LOG_INTERVAL 7U
#define OTA_MESH_SEGMENT_SIZE MESH_NETWORK_PAYLOAD_SIZE
#define OTA_MESH_TIMEOUT 20000U //in ms
#define OTA_ALLOW_REBOOT 1

#define ERROR_CHECK(x) if (err == ESP_OK)                                                               \
    {                                                                                                   \
        err = (x);                                                                                      \
        if (err != ESP_OK)                                                                              \
            {                                                                                           \
                ESP_LOGE(LOG_TAG,  "%s failed with error: 0x%x -> %s", #x,  err, esp_err_to_name(err)); \
            }                                                                                           \
    }                                                                                                   \

esp_err_t errMeshOTAInitialize(void);

//Tasks
void vMeshOtaTaskServerWorker(void *arg);
void vMeshOtaTaskOTAWorker(void *arg);

//OTA process endpoints
esp_err_t errMeshOtaSlaveEndpoint(bool* const cpbNewOTAImage, mesh_addr_t* const cpcMeshNodeAddr);
esp_err_t errMeshOtaMasterEndpoint(bool* const cpbNewOTAImage, const mesh_addr_t* const cpcMeshNodeAddr);

#endif /* H_MESH_OTA */
