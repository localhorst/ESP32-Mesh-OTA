/**
* @file Mesh_OTA_Util.h
* @brief Utility and helper functions to perfrom mesh OTA updates
* @author Hendrik Schutter
* @date 21.01.2021
*/

#ifndef H_MESH_OTA_UTIL
#define H_MESH_OTA_UTIL

#include "esp_system.h"
#include "esp_event.h"
#include "esp_log.h"

#include "Mesh_Network.h"
#include "HTTPS_Client.h"
#include "Mesh_OTA.h"

enum otaMeshRole
{
    Transmitter,
    Receiver
};

typedef  enum otaMeshRole OTA_MESH_ROLE_t;

//helper functions
bool bMeshOtaUtilNewerVersion(const char* cpu8Local, const char* cpu8Remote);
esp_err_t errMeshOtaUtilExtractVersionNumber(const char* cpu8Data, uint32_t* const cpu32DataLenght, char* const pc8RemoteVersionNumber);
esp_err_t errMeshOtaUtilFindImageStart(const  char* const cpu8Data, const uint32_t* const cpu32DataLenght, uint32_t* const cpu32StartOffset);
esp_err_t errMeshOtaUtilSendOtaVersionRequest(const mesh_addr_t* const cpcMeshReceiverAddr);
esp_err_t errMeshOtaUtilSendOtaVersionResponse(const mesh_addr_t* const cpcMeshReceiverAddr);
void vMeshOtaUtilPrintOtaProgress(const uint32_t* const cpcu32TotalImageSize, const uint32_t* const cpcu32BytesWritten, const OTA_MESH_ROLE_t ceRole);
void vMeshOtaUtilAddAllNeighboursToQueue(const mesh_addr_t* const cpcMeshNodeAddr);
void vMeshOtaUtilClearOtaMessageQueue(const mesh_addr_t* const cpcMeshNodeAddr);
void vMeshOtaUtilClearNeighboursQueue(const mesh_addr_t* const cpcMeshNodeAddr);

//Handler
void vMeshOtaUtilAddNodeToPossibleUpdatableQueue(const uint8_t* const cpcu8MAC);
void vMeshOtaUtilAddOtaMessageToQueue(const MESH_PACKET_t* const cpcuMeshPacket);
void vMeshOtaUtilChangeStateOfServerWorker(const bool cbState);

#endif /* H_MESH_OTA_UTIL */

