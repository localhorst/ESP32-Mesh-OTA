/**
* @file Mesh_OTA_Partition_Access.h
* @brief Write and read partition if requested from Mesh_OTA
* @author Hendrik Schutter
* @date 21.01.2021
*
* Additional Infos: Write image via HTTPS
*                   Receive or transmit via Mesh
*/

#ifndef H_MESH_OTA_PARTITION_ACCESS
#define H_MESH_OTA_PARTITION_ACCESS

#include "esp_system.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "esp_ota_ops.h"
#include "esp_partition.h"

#include "Mesh_Network.h"
#include "HTTPS_Client.h"

#define ERROR_CHECK(x) if (err == ESP_OK)                                                               \
    {                                                                                                   \
        err = (x);                                                                                      \
        if (err != ESP_OK)                                                                              \
            {                                                                                           \
                ESP_LOGE(LOG_TAG,  "%s failed with error: 0x%x -> %s", #x,  err, esp_err_to_name(err)); \
            }                                                                                           \
    }                                                                                                   \

esp_err_t errMeshOtaPartitionAccessHttps(bool* const cpbNewOTAImage);
esp_err_t errMeshOtaPartitionAccessMeshTransmit(const mesh_addr_t* const cpcMeshNodeAddr);
esp_err_t errMeshOtaPartitionAccessMeshReceive(bool* const cpbNewOTAImage, const mesh_addr_t* const cpcMeshNodeAddr);

#endif /* H_MESH_OTA_PARTITION_ACCESS */
