
/**
* @file Mesh_Network_Handler.c
* @brief Handler for events from mesh network (no messages)
* @author Hendrik Schutter
* @date 20.01.2021
*
* Additional Infos: IP event received or parrent or child connected or disconnected
*/

#include "Mesh_Network.h"

static const char *LOG_TAG = "mesh_network_handler";

/**
* @fn void vMeshNetworkIpEventHandler(void *arg, esp_event_base_t event_base, int32_t i32EventID, void *vpEventData)
* @brief received an IP event
* @param arg
* @param event_base
* @param i32EventID
* @param vpEventData
* @return void
* @author ESP-IDF
* @date 20.01.2021
*/
void vMeshNetworkIpEventHandler(void *arg, esp_event_base_t event_base, int32_t i32EventID, void *vpEventData)
{
    ip_event_got_ip_t *event = (ip_event_got_ip_t *) vpEventData;
    ESP_LOGI(LOG_TAG, "<IP_EVENT_STA_GOT_IP>IP:" IPSTR, IP2STR(&event->ip_info.ip));
    if(pChangeStateOfServerWorkerHandle)
        {
            pChangeStateOfServerWorkerHandle(true);   //signal that this node (root node) has access to internet
        }
}

/**
* @fn void vMeshNetworkMeshEventHandler(void *arg, esp_event_base_t event_base, int32_t i32EventID, void* vpEventData)
* @brief received an mesh event
* @param arg
* @param event_base
* @param i32EventID
* @param vpEventData
* @return void
* @author ESP-IDF
* @date 20.01.2021
*/
void vMeshNetworkMeshEventHandler(void *arg, esp_event_base_t event_base, int32_t i32EventID, void* vpEventData)
{
    mesh_addr_t id = {0,};
    static uint16_t last_layer = 0;

    switch (i32EventID)
        {
        case MESH_EVENT_STARTED:
        {
            esp_mesh_get_id(&id);
            ESP_LOGI(LOG_TAG, "<MESH_EVENT_MESH_STARTED>ID:"MACSTR"", MAC2STR(id.addr));
bIsMeshConnected = false;
i32MeshLayer = esp_mesh_get_layer();
}
break;
        case MESH_EVENT_STOPPED: 
{
ESP_LOGI(LOG_TAG, "<MESH_EVENT_STOPPED>");
bIsMeshConnected = false;
i32MeshLayer = esp_mesh_get_layer();
}
break;
        case MESH_EVENT_CHILD_CONNECTED: 
{
mesh_event_child_connected_t *child_connected = (mesh_event_child_connected_t *)vpEventData;
ESP_LOGI(LOG_TAG, "<MESH_EVENT_CHILD_CONNECTED>aid:%d, "MACSTR"", child_connected->aid, MAC2STR(child_connected->mac));

if(pOTAChildConnectHandle){pOTAChildConnectHandle(child_connected->mac);}//add this child to queue using handle

}
break;
        case MESH_EVENT_CHILD_DISCONNECTED: 
{
mesh_event_child_disconnected_t *child_disconnected = (mesh_event_child_disconnected_t *)vpEventData;
ESP_LOGI(LOG_TAG, "<MESH_EVENT_CHILD_DISCONNECTED>aid:%d, "MACSTR"",
child_disconnected->aid,
MAC2STR(child_disconnected->mac));
}
break;
        case MESH_EVENT_ROUTING_TABLE_ADD: 
{
mesh_event_routing_table_change_t *routing_table = (mesh_event_routing_table_change_t *)vpEventData;
ESP_LOGW(LOG_TAG, "<MESH_EVENT_ROUTING_TABLE_ADD>add %d, new:%d, layer:%d",
routing_table->rt_size_change,
routing_table->rt_size_new, i32MeshLayer);
}
break;
        case MESH_EVENT_ROUTING_TABLE_REMOVE: 
{   
mesh_event_routing_table_change_t *routing_table = (mesh_event_routing_table_change_t *)vpEventData;
ESP_LOGW(LOG_TAG, "<MESH_EVENT_ROUTING_TABLE_REMOVE>remove %d, new:%d, layer:%d",
routing_table->rt_size_change,
routing_table->rt_size_new, i32MeshLayer);
}
break;
        case MESH_EVENT_NO_PARENT_FOUND: 
{
mesh_event_no_parent_found_t *no_parent = (mesh_event_no_parent_found_t *)vpEventData;
ESP_LOGI(LOG_TAG, "<MESH_EVENT_NO_PARENT_FOUND>scan times:%d",
no_parent->scan_times);
/* TODO handler for the failure, maybe nominate themselves */
}
break;
        case MESH_EVENT_PARENT_CONNECTED: 
{
mesh_event_connected_t *connected = (mesh_event_connected_t *)vpEventData;
esp_mesh_get_id(&id);
i32MeshLayer = connected->self_layer;
memcpy(&meshParentAddr.addr, connected->connected.bssid, 6); 
ESP_LOGI(LOG_TAG, "<MESH_EVENT_PARENT_CONNECTED>layer:%d-->%d, parent:"MACSTR"%s, ID:"MACSTR", duty:%d",
last_layer, i32MeshLayer, MAC2STR(meshParentAddr.addr),
esp_mesh_is_root() ? "<ROOT>" : (i32MeshLayer == 2) ? "<layer2>" : "", //print own node title
MAC2STR(id.addr), connected->duty);
last_layer = i32MeshLayer;
bIsMeshConnected = true;
if (esp_mesh_is_root()) 
{
if(esp_netif_dhcpc_start(pNetifSta) == ESP_ERR_ESP_NETIF_DHCP_ALREADY_STARTED) //get a IP from router
{
if(pChangeStateOfServerWorkerHandle){pChangeStateOfServerWorkerHandle(true);}// signal reconnect
} 
}
errMeshNetworkStartReceiveTask();//start receiving
}
break;
        case MESH_EVENT_PARENT_DISCONNECTED: 
{
mesh_event_disconnected_t *disconnected = (mesh_event_disconnected_t *)vpEventData;
ESP_LOGI(LOG_TAG, "<MESH_EVENT_PARENT_DISCONNECTED>reason:%d", disconnected->reason);
bIsMeshConnected = false;
if(pChangeStateOfServerWorkerHandle){pChangeStateOfServerWorkerHandle(false);}
i32MeshLayer = esp_mesh_get_layer();
}
break;
        case MESH_EVENT_LAYER_CHANGE: 
{
mesh_event_layer_change_t *layer_change = (mesh_event_layer_change_t *)vpEventData;
i32MeshLayer = layer_change->new_layer;
ESP_LOGI(LOG_TAG, "<MESH_EVENT_LAYER_CHANGE>layer:%d-->%d%s",
last_layer, i32MeshLayer,
esp_mesh_is_root() ? "<ROOT>" : (i32MeshLayer == 2) ? "<layer2>" : "");
last_layer = i32MeshLayer;
}
break;
        case MESH_EVENT_ROOT_ADDRESS: 
{
mesh_event_root_address_t *root_addr = (mesh_event_root_address_t *)vpEventData;
ESP_LOGI(LOG_TAG, "<MESH_EVENT_ROOT_ADDRESS>root address:"MACSTR"",
MAC2STR(root_addr->addr));
}
break;
        case MESH_EVENT_VOTE_STARTED: 
{
mesh_event_vote_started_t *vote_started = (mesh_event_vote_started_t *)vpEventData;
ESP_LOGI(LOG_TAG, "<MESH_EVENT_VOTE_STARTED>attempts:%d, reason:%d, rc_addr:"MACSTR"",
vote_started->attempts,
vote_started->reason,
MAC2STR(vote_started->rc_addr.addr));
}
break;
        case MESH_EVENT_VOTE_STOPPED: 
{
ESP_LOGI(LOG_TAG, "<MESH_EVENT_VOTE_STOPPED>");
}
break;
        case MESH_EVENT_ROOT_SWITCH_REQ: 
{
mesh_event_root_switch_req_t *switch_req = (mesh_event_root_switch_req_t *)vpEventData;
ESP_LOGI(LOG_TAG, "<MESH_EVENT_ROOT_SWITCH_REQ>reason:%d, rc_addr:"MACSTR"", switch_req->reason,
MAC2STR( switch_req->rc_addr.addr));
}
break;
        case MESH_EVENT_ROOT_SWITCH_ACK: 
{
//new root
i32MeshLayer = esp_mesh_get_layer();
esp_mesh_get_parent_bssid(&meshParentAddr);
ESP_LOGI(LOG_TAG, "<MESH_EVENT_ROOT_SWITCH_ACK>layer:%d, parent:"MACSTR"", i32MeshLayer, MAC2STR(meshParentAddr.addr));
}
break;
        case MESH_EVENT_TODS_STATE: 
{
mesh_event_toDS_state_t *toDs_state = (mesh_event_toDS_state_t *)vpEventData;
ESP_LOGI(LOG_TAG, "<MESH_EVENT_TODS_REACHABLE>state:%d", *toDs_state);
}
break;
        case MESH_EVENT_ROOT_FIXED: 
{
mesh_event_root_fixed_t *root_fixed = (mesh_event_root_fixed_t *)vpEventData;
ESP_LOGI(LOG_TAG, "<MESH_EVENT_ROOT_FIXED>%s",
root_fixed->is_fixed ? "fixed" : "not fixed");
}
break;
        case MESH_EVENT_ROOT_ASKED_YIELD: 
{
mesh_event_root_conflict_t *root_conflict = (mesh_event_root_conflict_t *)vpEventData;
ESP_LOGI(LOG_TAG, "<MESH_EVENT_ROOT_ASKED_YIELD>"MACSTR", rssi:%d, capacity:%d",
MAC2STR(root_conflict->addr), root_conflict->rssi, root_conflict->capacity);
}
break;
        case MESH_EVENT_CHANNEL_SWITCH: 
{
mesh_event_channel_switch_t *channel_switch = (mesh_event_channel_switch_t *)vpEventData;
ESP_LOGI(LOG_TAG, "<MESH_EVENT_CHANNEL_SWITCH>new channel:%d", channel_switch->channel);
}
break;
        case MESH_EVENT_SCAN_DONE: 
{
mesh_event_scan_done_t *scan_done = (mesh_event_scan_done_t *)vpEventData;
ESP_LOGI(LOG_TAG, "<MESH_EVENT_SCAN_DONE>number:%d", scan_done->number);
}
break;
        case MESH_EVENT_NETWORK_STATE: 
{
mesh_event_network_state_t *network_state = (mesh_event_network_state_t *)vpEventData;
ESP_LOGI(LOG_TAG, "<MESH_EVENT_NETWORK_STATE>is_rootless:%d", network_state->is_rootless);
}
break;
        case MESH_EVENT_STOP_RECONNECTION: 
{
ESP_LOGI(LOG_TAG, "<MESH_EVENT_STOP_RECONNECTION>");    
}
break;
        case MESH_EVENT_FIND_NETWORK: 
{
mesh_event_find_network_t *find_network = (mesh_event_find_network_t *)vpEventData;
ESP_LOGI(LOG_TAG, "<MESH_EVENT_FIND_NETWORK>new channel:%d, router BSSID:"MACSTR"",
find_network->channel, MAC2STR(find_network->router_bssid));
}
break;
        case MESH_EVENT_ROUTER_SWITCH: 
{
mesh_event_router_switch_t *router_switch = (mesh_event_router_switch_t *)vpEventData;
ESP_LOGI(LOG_TAG, "<MESH_EVENT_ROUTER_SWITCH>new router:%s, channel:%d, "MACSTR"",
router_switch->ssid, router_switch->channel, MAC2STR(router_switch->bssid));
}
break;
        case MESH_EVENT_PS_PARENT_DUTY: 
{
mesh_event_ps_duty_t *ps_duty = (mesh_event_ps_duty_t *)vpEventData;
ESP_LOGI(LOG_TAG, "<MESH_EVENT_PS_PARENT_DUTY>duty:%d", ps_duty->duty);
}
break;
        case MESH_EVENT_PS_CHILD_DUTY: 
{
mesh_event_ps_duty_t *ps_duty = (mesh_event_ps_duty_t *)vpEventData;
ESP_LOGI(LOG_TAG, "<MESH_EVENT_PS_CHILD_DUTY>cidx:%d, "MACSTR", duty:%d", ps_duty->child_connected.aid-1,
MAC2STR(ps_duty->child_connected.mac), ps_duty->duty);
}
break;
        default:
            ESP_LOGI(LOG_TAG, "unknown id:%d", i32EventID);
        break;
    }
}
