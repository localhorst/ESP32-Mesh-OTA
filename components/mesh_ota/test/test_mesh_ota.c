#include <limits.h>
#include "unity.h"

#include "Mesh_OTA_Util.h"
#include "test_image_hex.h"
#include "Mesh_Network.h"

// ### ### ###  distinguish newer image version ### ### ###

TEST_CASE("Remote got patch", "[distinguish newer image version]")
{
    char versionLocal[] = "1.2.3";  //current running image
    char versionRemote[] = "1.2.4"; //image from server
    TEST_ASSERT_TRUE( bMeshOtaUtilNewerVersion(versionLocal, versionRemote)  );
}

TEST_CASE("Remote got minor", "[distinguish newer image version]")
{
    char versionLocal[] = "1.2.3";  //current running image
    char versionRemote[] = "1.3.3"; //image from server
    TEST_ASSERT_TRUE( bMeshOtaUtilNewerVersion(versionLocal, versionRemote)  );
}

TEST_CASE("Remote got major", "[distinguish newer image version]")
{
    char versionLocal[] = "1.2.3";  //current running image
    char versionRemote[] = "2.2.3"; //image from server
    TEST_ASSERT_TRUE( bMeshOtaUtilNewerVersion(versionLocal, versionRemote)  );
}

TEST_CASE("Local got patch", "[distinguish newer image version]")
{
    char versionLocal[] = "1.2.4";  //current running image
    char versionRemote[] = "1.2.3"; //image from server
    TEST_ASSERT_FALSE( bMeshOtaUtilNewerVersion(versionLocal, versionRemote)  );
}

TEST_CASE("Local got minor", "[distinguish newer image version]")
{
    char versionLocal[] = "1.3.3";  //current running image
    char versionRemote[] = "1.2.3"; //image from server
    TEST_ASSERT_FALSE( bMeshOtaUtilNewerVersion(versionLocal, versionRemote)  );
}

TEST_CASE("Local got major", "[distinguish newer image version]")
{
    char versionLocal[] = "2.2.3";  //current running image
    char versionRemote[] = "1.2.3"; //image from server
    TEST_ASSERT_FALSE( bMeshOtaUtilNewerVersion(versionLocal, versionRemote)  );
}

TEST_CASE("Remote got alpha and patch", "[distinguish newer image version]")
{
    char versionLocal[] = "2.2.3";  //current running image
    char versionRemote[] = "a2.2.4"; //image from server
    TEST_ASSERT_TRUE( bMeshOtaUtilNewerVersion(versionLocal, versionRemote)  );
}

TEST_CASE("Remote got max", "[distinguish newer image version]")
{
    char versionLocal[] = "2.2.3";  //current running image
    char versionRemote[] = "999.999.999"; //image from server
    TEST_ASSERT_TRUE( bMeshOtaUtilNewerVersion(versionLocal, versionRemote)  );
}

// ### ### ###  find start offset in firmware image ### ### ###

TEST_CASE("with http response +  0.0.1", "[find start offset in firmware image]")
{
    uint32_t u32StartOffset;
    uint32_t u32DataLenght = sizeof(dataWithHttpRespone0_0_1)/sizeof(dataWithHttpRespone0_0_1[0]);
    esp_err_t err = errMeshOtaUtilFindImageStart(dataWithHttpRespone0_0_1, &u32DataLenght, &u32StartOffset);
    if(err == ESP_OK)
    {
        TEST_ASSERT_EQUAL_INT(305, u32StartOffset);
    }
    else
    {
        TEST_ASSERT_EQUAL_INT(ESP_OK, err);
    }
}

TEST_CASE("without http response +  0.0.1", "[find start offset in firmware image]")
{
    uint32_t u32StartOffset;
    uint32_t u32DataLenght = sizeof(dataWithoutHttpRespone0_0_1)/sizeof(dataWithoutHttpRespone0_0_1[0]);
    esp_err_t err =  errMeshOtaUtilFindImageStart(dataWithoutHttpRespone0_0_1, &u32DataLenght, &u32StartOffset);
    if(err == ESP_OK)
    {
        TEST_ASSERT_EQUAL_INT(0, u32StartOffset);
    }
    else
    {
        TEST_ASSERT_EQUAL_INT(ESP_OK, err);
    }
}


TEST_CASE("with http response +  999.999.999", "[find start offset in firmware image]")
{
    uint32_t u32StartOffset;
    uint32_t u32DataLenght = sizeof(dataWithHttpRespone999_999_999)/sizeof(dataWithHttpRespone999_999_999[0]);
    esp_err_t err = errMeshOtaUtilFindImageStart(dataWithHttpRespone999_999_999, &u32DataLenght, &u32StartOffset);
    if(err == ESP_OK)
    {
        TEST_ASSERT_EQUAL_INT(305, u32StartOffset);
    }
    else
    {
        TEST_ASSERT_EQUAL_INT(ESP_OK, err);
    }
}

TEST_CASE("without http response +  999.999.999", "[find start offset in firmware image]")
{
    uint32_t u32StartOffset;
    uint32_t u32DataLenght = sizeof(dataWithoutHttpRespone999_999_999)/sizeof(dataWithoutHttpRespone999_999_999[0]);
    esp_err_t err = errMeshOtaUtilFindImageStart(dataWithoutHttpRespone999_999_999, &u32DataLenght, &u32StartOffset);
    if(err == ESP_OK)
    {
        TEST_ASSERT_EQUAL_INT(0, u32StartOffset);
    }
    else
    {
        TEST_ASSERT_EQUAL_INT(ESP_OK, err);
    }
}

TEST_CASE("with http response +  999.9.999", "[find start offset in firmware image]")
{
    uint32_t u32StartOffset;
    uint32_t u32DataLenght = sizeof(dataWithHttpRespone999_9_999)/sizeof(dataWithHttpRespone999_9_999[0]);
    esp_err_t err = errMeshOtaUtilFindImageStart(dataWithHttpRespone999_9_999, &u32DataLenght, &u32StartOffset);
    if(err == ESP_OK)
    {
        TEST_ASSERT_EQUAL_INT(302, u32StartOffset);
    }
    else
    {
        TEST_ASSERT_EQUAL_INT(ESP_OK, err);
    }
}

TEST_CASE("with http response +  999.99.999", "[find start offset in firmware image]")
{
    uint32_t u32StartOffset;
    uint32_t u32DataLenght = sizeof(dataWithHttpRespone999_99_999)/sizeof(dataWithHttpRespone999_99_999[0]);
    esp_err_t err = errMeshOtaUtilFindImageStart(dataWithHttpRespone999_99_999, &u32DataLenght, &u32StartOffset);
    if(err == ESP_OK)
    {
        TEST_ASSERT_EQUAL_INT(299, u32StartOffset);
    }
    else
    {
        TEST_ASSERT_EQUAL_INT(ESP_OK, err);
    }
}

// ### ### ###  distinguish newer image version ### ### ###

TEST_CASE("extract version 0.0.1", "[extract image version number]")
{
    char versionLocal[] = "0.0.0";  //current running image
    char versionRemote[12];//image from server
    uint32_t u32DataLenght = sizeof(dataWithHttpRespone0_0_1)/sizeof(dataWithHttpRespone0_0_1[0]);
    esp_err_t err = errMeshOtaUtilExtractVersionNumber(dataWithHttpRespone0_0_1, &u32DataLenght, versionRemote);
    if(err == ESP_OK)
    {
        TEST_ASSERT_TRUE( bMeshOtaUtilNewerVersion(versionLocal, versionRemote)  );
    }
    else
    {
        TEST_ASSERT_EQUAL_INT(ESP_OK, err);
    }
}

TEST_CASE("extract version 999.999.999", "[extract image version number]")
{
    char versionLocal[] = "0.0.0";  //current running image
    char versionRemote[12];//image from server
    uint32_t u32DataLenght = sizeof(dataWithHttpRespone999_999_999)/sizeof(dataWithHttpRespone999_999_999[0]);
    esp_err_t err = errMeshOtaUtilExtractVersionNumber(dataWithHttpRespone999_999_999, &u32DataLenght, versionRemote);
    if(err == ESP_OK)
    {
        TEST_ASSERT_TRUE( bMeshOtaUtilNewerVersion(versionLocal, versionRemote)  );
    }
    else
    {
        TEST_ASSERT_EQUAL_INT(ESP_OK, err);
    }
}

TEST_CASE("extract version 999.99.999", "[extract image version number]")
{
    char versionLocal[] = "999.100.999";  //current running image
    char versionRemote[12];//image from server
    uint32_t u32DataLenght = sizeof(dataWithHttpRespone999_99_999)/sizeof(dataWithHttpRespone999_99_999[0]);
    esp_err_t err = errMeshOtaUtilExtractVersionNumber(dataWithHttpRespone999_99_999, &u32DataLenght, versionRemote);
    if(err == ESP_OK)
    {
        TEST_ASSERT_FALSE( bMeshOtaUtilNewerVersion(versionLocal, versionRemote)  );
    }
    else
    {
        TEST_ASSERT_EQUAL_INT(ESP_OK, err);
    }
}


TEST_CASE("same MACs", "[check MAC equality]")
{
   unsigned char cMAC_A[] = "012345678901";  
   unsigned char cMAC_B[] = "012345678901";  

    TEST_ASSERT_TRUE( bMeshNetworkCheckMacEquality(cMAC_A,cMAC_B)  );
}

TEST_CASE("different MACs", "[check MAC equality]")
{
   unsigned char cMAC_A[] = "012345678901";  
   unsigned char cMAC_B[] = "087464874718";  

    TEST_ASSERT_FALSE( bMeshNetworkCheckMacEquality(cMAC_A,cMAC_B)  );
}

TEST_CASE("same MACs last A_byte+1", "[check MAC equality]")
{
   unsigned char cMAC_A[] = "012345678902";  
   unsigned char cMAC_B[] = "012345678901"; 

    TEST_ASSERT_TRUE( bMeshNetworkCheckMacEquality(cMAC_A,cMAC_B)  );
}

TEST_CASE("same MACs last B_byte+1", "[check MAC equality]")
{
   unsigned char cMAC_A[] = "012345678901";  
   unsigned char cMAC_B[] = "012345678902"; 

    TEST_ASSERT_TRUE( bMeshNetworkCheckMacEquality(cMAC_A,cMAC_B)  );
}

TEST_CASE("same MACs last B_byte+2", "[check MAC equality]")
{
   unsigned char cMAC_A[] = "012345678901";  
   unsigned char cMAC_B[] = "012345678903"; 

    TEST_ASSERT_TRUE( bMeshNetworkCheckMacEquality(cMAC_A,cMAC_B)  );
}

TEST_CASE("same Node0", "[check MAC equality]")
{
   unsigned char cMAC_A[] = "ac67b27162b0";  
   unsigned char cMAC_B[] = "ac67b27162b0";   

    TEST_ASSERT_TRUE( bMeshNetworkCheckMacEquality(cMAC_A,cMAC_B)  );
}

TEST_CASE("same Node0 first MAC+1", "[check MAC equality]")
{
   unsigned char cMAC_A[] = "ac67b27162b1";  
   unsigned char cMAC_B[] = "ac67b27162b0";   

    TEST_ASSERT_TRUE( bMeshNetworkCheckMacEquality(cMAC_A,cMAC_B)  );
}

TEST_CASE("same Node0 second MAC+1", "[check MAC equality]")
{
   unsigned char cMAC_A[] = "ac67b27162b0";  
   unsigned char cMAC_B[] = "ac67b27162b1";   

    TEST_ASSERT_TRUE( bMeshNetworkCheckMacEquality(cMAC_A,cMAC_B)  );
}










